<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
   <xsl:template match="/content">
      <html>
         <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title>
               <xsl:value-of select="./text/sbirka/nadpis" />
               -
               <xsl:value-of select="./text/spisovatel" />
            </title>
            <link rel="stylesheet" type="text/css" href="./sbirka.css" />
            <script src="utility.js" />
         </head>
         <div class="body">
            <div class="content">
               <div class="obsah_odkazy">
                  <ol>
                     <xsl:for-each select="//basen">
                        <xsl:variable name="basen_id">
                           <xsl:number level="any" count="basen" />
                        </xsl:variable>
                        <li>
                           <a href="#basen_{$basen_id}">
                              <xsl:value-of select="./nadpis" />
                           </a>
                        </li>
                     </xsl:for-each>
                  </ol>
               </div>
               <xsl:apply-templates select="./text" mode="normal" />
            </div>
         </div>
      </html>
   </xsl:template>
   <xsl:template match="dilo" mode="normal">
      <div class="dilo">
         <xsl:copy-of select="./@*" />
         <xsl:apply-templates select="./*" mode="normal" />
      </div>
   </xsl:template>
   <xsl:template match="hlavicka" mode="normal">
      <div class="hlavicka">
         <xsl:copy-of select="./@*" />
         <xsl:apply-templates select="./*" mode="normal" />
      </div>
   </xsl:template>
   <xsl:template match="komentare" mode="normal">
      <div class="komentare">
         <xsl:copy-of select="./@*" />
         <xsl:apply-templates select="./*" mode="normal" />
      </div>
   </xsl:template>
   <xsl:template match="text" mode="normal">
      <div class="text">
         <xsl:copy-of select="./@*" />
         <xsl:apply-templates select="./*" mode="normal" />
      </div>
   </xsl:template>
   <xsl:template match="kniha" mode="normal">
      <div class="kniha">
         <xsl:copy-of select="./@*" />
         <xsl:apply-templates select="./*" mode="normal" />
      </div>
   </xsl:template>
   <xsl:template match="basen" mode="normal">
      <div class="basen">
         <xsl:copy-of select="./@*" />
         <xsl:variable name="basen_id">
            <xsl:number level="any" count="basen" />
         </xsl:variable>
         <a name="basen_{$basen_id}" />
         <xsl:apply-templates select="./*" mode="normal" />
      </div>
   </xsl:template>
   <xsl:template match="fales-titulbasen" mode="normal">
      <div class="fales-titulbasen">
         <xsl:copy-of select="./@*" />
         <xsl:apply-templates select="./*" mode="normal" />
      </div>
   </xsl:template>
   <xsl:template match="nadpisvbasni" mode="normal">
      <div class="nadpisvbasni">
         <xsl:copy-of select="./@*" />
         <xsl:apply-templates select="./*" mode="normal" />
      </div>
   </xsl:template>
   <xsl:template match="obsah" mode="normal">
      <div class="obsah">
         <xsl:copy-of select="./@*" />
         <xsl:apply-templates select="./*" mode="normal" />
      </div>
   </xsl:template>
   <xsl:template match="oddilbasne" mode="normal">
      <div class="oddilbasne">
         <xsl:copy-of select="./@*" />
         <xsl:apply-templates select="./*" mode="normal" />
      </div>
   </xsl:template>
   <xsl:template match="strofa" mode="normal">
      <div class="strofa">
         <xsl:copy-of select="./@*" />
         <xsl:apply-templates select="./*" mode="normal" />
      </div>
   </xsl:template>
   <xsl:template match="podtitulbasne" mode="normal">
      <div class="podtitulbasne">
         <xsl:copy-of select="./@*" />
         <xsl:apply-templates select="./*" mode="normal" />
      </div>
   </xsl:template>
   <xsl:template match="sifra" mode="normal">
      <div class="sifra">
         <xsl:copy-of select="./@*" />
         <xsl:apply-templates select="./*" mode="normal" />
      </div>
   </xsl:template>
   <xsl:template match="oddil" mode="normal">
      <div class="oddil">
         <xsl:copy-of select="./@*" />
         <xsl:apply-templates select="./*" mode="normal" />
      </div>
   </xsl:template>
   <xsl:template match="pododdil" mode="normal">
      <div class="pododdil">
         <xsl:copy-of select="./@*" />
         <xsl:apply-templates select="./*" mode="normal" />
      </div>
   </xsl:template>
   <xsl:template match="fales-tituloddil" mode="normal">
      <div class="fales-tituloddil">
         <xsl:copy-of select="./@*" />
         <xsl:apply-templates select="./*" mode="normal" />
      </div>
   </xsl:template>
   <xsl:template match="podtitulsbirky" mode="normal">
      <div class="podtitulsbirky">
         <xsl:copy-of select="./@*" />
         <xsl:apply-templates select="./*" mode="normal" />
      </div>
   </xsl:template>
   <xsl:template match="sbirka" mode="normal">
      <div class="sbirka">
         <xsl:copy-of select="./@*" />
         <xsl:apply-templates select="./*" mode="normal" />
      </div>
   </xsl:template>
   <xsl:template match="autor" mode="normal">
      <div class="autor">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="autor" mode="mixed">
      <div class="autor">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="autormota" mode="normal">
      <div class="autormota">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="autormota" mode="mixed">
      <div class="autormota">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="edicnipoznamka" mode="normal">
      <div class="edicnipoznamka">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="edicnipoznamka" mode="mixed">
      <div class="edicnipoznamka">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="i" mode="normal">
      <div class="i">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="i" mode="mixed">
      <div class="i">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="format" mode="normal">
      <div class="format">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="format" mode="mixed">
      <div class="format">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="komentar" mode="normal">
      <div class="komentar">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="komentar" mode="mixed">
      <div class="komentar">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="misto" mode="normal">
      <div class="misto">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="misto" mode="mixed">
      <div class="misto">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="moto" mode="normal">
      <div class="moto">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="moto" mode="mixed">
      <div class="moto">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="podtitul" mode="normal">
      <div class="podtitul">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="podtitul" mode="mixed">
      <div class="podtitul">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="popis" mode="normal">
      <div class="popis">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="popis" mode="mixed">
      <div class="popis">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="rok" mode="normal">
      <div class="rok">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="rok" mode="mixed">
      <div class="rok">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="stran" mode="normal">
      <div class="stran">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="stran" mode="mixed">
      <div class="stran">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="titul" mode="normal">
      <div class="titul">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="titul" mode="mixed">
      <div class="titul">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="venovani" mode="normal">
      <div class="venovani">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="venovani" mode="mixed">
      <div class="venovani">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="vydani" mode="normal">
      <div class="vydani">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="vydani" mode="mixed">
      <div class="vydani">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="vydavatel" mode="normal">
      <div class="vydavatel">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="vydavatel" mode="mixed">
      <div class="vydavatel">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="zdroj-signatura" mode="normal">
      <div class="zdroj-signatura">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="zdroj-signatura" mode="mixed">
      <div class="zdroj-signatura">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="doprovod" mode="normal">
      <div class="doprovod">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="doprovod" mode="mixed">
      <div class="doprovod">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="verze" mode="normal">
      <div class="verze">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="verze" mode="mixed">
      <div class="verze">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="b" mode="normal">
      <div class="b">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="b" mode="mixed">
      <div class="b">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="moto-autor" mode="normal">
      <div class="moto-autor">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="moto-autor" mode="mixed">
      <div class="moto-autor">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="moto-text" mode="normal">
      <div class="moto-text">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="moto-text" mode="mixed">
      <div class="moto-text">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="nadpis" mode="normal">
      <div class="nadpis">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="nadpis" mode="mixed">
      <div class="nadpis">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="vlevo" mode="normal">
      <div class="vlevo">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="vlevo" mode="mixed">
      <div class="vlevo">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="vpravo" mode="normal">
      <div class="vpravo">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="vpravo" mode="mixed">
      <div class="vpravo">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="spisovatel" mode="normal">
      <div class="spisovatel">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="spisovatel" mode="mixed">
      <div class="spisovatel">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="strana" mode="normal">
      <div class="strana">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
      <hr />
   </xsl:template>
   <xsl:template match="strana" mode="mixed">
      <div class="strana">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
      <hr />
   </xsl:template>
   <xsl:template match="v" mode="normal">
      <l>
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </l>
   </xsl:template>
   <xsl:template match="v" mode="mixed">
      <l>
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </l>
   </xsl:template>
   <xsl:template match="prolozeno" mode="normal">
      <div class="prolozeno">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="prolozeno" mode="mixed">
      <div class="prolozeno">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="zakladnitext" mode="normal">
      <div class="zakladnitext">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="zakladnitext" mode="mixed">
      <div class="zakladnitext">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="poznpodcarou" mode="normal">
      <div class="poznpodcarou">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="poznpodcarou" mode="mixed">
      <div class="poznpodcarou">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="sup" mode="normal">
      <div class="sup">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="sup" mode="mixed">
      <div class="sup">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="tiraz" mode="normal">
      <div class="tiraz">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="tiraz" mode="mixed">
      <div class="tiraz">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="titulsbirka" mode="normal">
      <div class="titulsbirka">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="titulsbirka" mode="mixed">
      <div class="titulsbirka">
         <xsl:copy-of select="./@*" />
         <xsl:for-each select=".">
            <xsl:apply-templates mode="mixed" />
         </xsl:for-each>
      </div>
   </xsl:template>
   <xsl:template match="br" mode="normal">
      <div class="br">
         <xsl:value-of select="." />
      </div>
   </xsl:template>
   <xsl:template match="br" mode="mixed">
      <div class="br">
         <xsl:value-of select="." />
      </div>
   </xsl:template>
   <xsl:template match="nbsp" mode="normal">
      <div class="nbsp">
         <xsl:value-of select="." />
      </div>
   </xsl:template>
   <xsl:template match="nbsp" mode="mixed">
      <div class="nbsp">
         <xsl:value-of select="." />
      </div>
   </xsl:template>
   <xsl:template match="tab" mode="normal">
      <div class="tab">
         <xsl:value-of select="." />
      </div>
   </xsl:template>
   <xsl:template match="tab" mode="mixed">
      <div class="tab">
         <xsl:value-of select="." />
      </div>
   </xsl:template>
   <xsl:template match="datum" mode="normal">
      <div class="datum">
         <xsl:value-of select="." />
      </div>
   </xsl:template>
   <xsl:template match="datum" mode="mixed">
      <div class="datum">
         <xsl:value-of select="." />
      </div>
   </xsl:template>
   <xsl:template match="*" mode="mixed">
      <xsl:copy>
         <xsl:copy-of select="@*" />
         <xsl:apply-templates mode="mixed" />
      </xsl:copy>
   </xsl:template>
   <xsl:template match="komentar" mode="mixed">
      <xsl:variable name="koment_id" select="@id" />
      <span class="komentar">
         <xsl:copy-of select="@*" />
         <xsl:apply-templates mode="mixed" />
         <span class="tooltip">
            <xsl:copy-of select="/content/komentare/komentar[@id=$koment_id]/text" />
         </span>
      </span>
   </xsl:template>
   <xsl:template match="verze" mode="mixed">
      <span class="verze">
         <xsl:copy-of select="@*" />
         <xsl:apply-templates mode="mixed" />
      </span>
   </xsl:template>
</xsl:stylesheet>
